#!/conda/bin/python

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import sys
import math

sys.path.append("/home/ckrug/compiles/oa50p083/oaScript_swig/python")
import oa

## OA uses radians, range is from 0 to 2*pi.
## Qt uses 16*360 degrees == 5760
QT_FULLCIRCLE = 5760.0
QT_DEGREE_SCALE = 2880.0 / math.pi

class OASymbolViewer(QWidget):
    """Widget that displays an OA symbol"""
    def __init__(self, parent=None):
        super(OASymbolViewer, self).__init__(parent)
        self.design = None
        self.draw = False
        self.setMinimumSize(200, 200)
        self.setMaximumSize(200, 200)
        
    def setDesign(self, lib, cell, view):
        """ opens an OA design for display, rejects all views except symbol """
        ## TODO: this might be inefficient using open all the time
        if view == "symbol":
            if oa.oaDesign.exists(lib, cell, view):
                self.design = oa.oaDesign.open(lib, cell, view, "r")
            else:
                self.design = None
            self.draw = self.design is not None and self.design.isValid()
        else:
            self.draw = False
            
    def paintEvent(self, event):
        """ This is called by Qt whenever the widget needs to draw itself """
        ##print("OASymbolViewer paintEvent")
        painter = QPainter(self)
        if not self.draw:
            painter.setPen(Qt.red)
            painter.drawLine(0, 0, self.width(), self.height())
            painter.drawLine(0, self.height(), self.width(), 0)
            return
        
        bb = self.design.getTopBlock().getBBox()
        
        wscale = float(self.width())  / float(bb.getWidth())
        hscale = float(self.height()) / float(bb.getHeight())
        scale = min(wscale, hscale)
        ## go to the center of the control
        painter.translate(self.width() / 2.0, self.height() / 2.0)
        
        ## change scale to fit on control, y is inverted for
        ## lower-left origin (Qt defaults upper left origin)
        painter.scale(scale, -scale)
        ## move to new origin
        painter.translate(-(bb.left()+bb.right())/2.0, -(bb.bottom()+bb.top())/2.0)
        
        ## paint the background white
        painter.setPen(Qt.transparent)
        painter.setBrush(Qt.white)
        painter.drawRect(bb.left(), bb.bottom(), bb.getWidth(), bb.getHeight())
        
        painter.setPen(Qt.black)
        painter.setBrush(Qt.transparent)
        
        ## paint the cell's shapes
        for s in self.design.getTopBlock().getShapes():
            t = int(s.getType())
            if t == oa.oacRectType:
                bb = s.getBBox()
                painter.drawRect(bb.left(), bb.bottom(), bb.getWidth(), bb.getHeight())
            elif t == oa.oacLineType:
                pa = oa.oaPointArray()
                s.getPoints(pa)
                p0 = QPoint(pa[0][0], pa[0][1])
                for p in pa[1:]:
                    p1 = QPoint(p[0], p[1])
                    painter.drawLine(p0, p1)
                    p0 = p1
            elif t == oa.oacArcType:
                ## 
                bb = s.getEllipseBBox()
                span = (s.getStopAngle()-s.getStartAngle())*QT_DEGREE_SCALE
                if span < 0:
                    span += QT_FULL_CIRCLE
                painter.drawArc(bb.left(), bb.bottom(), bb.getWidth(), bb.getHeight(),
                                QT_FULL_CIRCLE-(s.getStopAngle()*QT_DEGREE_SCALE), span)
            elif t == oa.oacEllipseType:
                bb = s.getBBox()
                painter.drawEllipse(bb.left(), bb.bottom(), bb.getWidth(), bb.getHeight())
        
        ## paint the cell's terminals
        painter.setPen(Qt.red)        
        for pt in self.design.getTopBlock().getTerms():
            for p in pt.getPins():
                for s in p.getFigs():
                    t = int(s.getType())
                    if t == oa.oacRectType:
                        bb = s.getBBox()
                        painter.drawRect(bb.left(), bb.bottom(), bb.getWidth(), bb.getHeight())

class OASymbolTestDialog(QDialog):
    """ A small dialog to test the widget, gives menus to select lib and cell """
    def __init__(self, parent=None):
        super(OASymbolTestDialog, self).__init__(parent)
        
        libLabel  = QLabel("&LibName:")
        cellLabel = QLabel("&CellName:")
        
        libnames = [i.getName() for i in oa.oaLib.getOpenLibs()]
        self.libEdit  = QComboBox()
        self.libEdit.addItems(libnames)
        self.cellEdit = QComboBox()
        
        libLabel.setBuddy(self.libEdit)
        cellLabel.setBuddy(self.cellEdit)
        
        self.libEdit.currentTextChanged.connect(self.loadCellNames)
        self.cellEdit.currentTextChanged.connect(self.showCellSymbol)
        
        ## create the widget
        self.symview = OASymbolViewer(self)
        self.symview.resize(200, 200)
        
        ## layout the buttons and widget
        blayout = QGridLayout()
        blayout.addWidget(libLabel,      0, 0)
        blayout.addWidget(self.libEdit,  1, 0)
        blayout.addWidget(cellLabel,     2, 0)
        blayout.addWidget(self.cellEdit, 3, 0)
        layout = QHBoxLayout()
        layout.addLayout(blayout)
        layout.addWidget(self.symview)
        self.setLayout(layout)

        ## do an initial load of the cells menu
        self.loadCellNames(libnames[0])

    def loadCellNames(self, libname):
        self.cellEdit.clear()
        lib = oa.oaLib.find(str(libname))
        if lib:
            self.cellEdit.addItems(sorted([i.getName() for i in lib.getCells()]))
            
    def showCellSymbol(self, cellname):
        self.symview.setDesign(str(self.libEdit.currentText()), str(self.cellEdit.currentText()), "symbol")
        self.symview.update()

if __name__ == "__main__":
    print("starting test dialog")

    ## for testing purposes, we expect a local lib.defs file to specify libraries
    oa.oaLibDefList.openLibs("lib.defs")
    for lib in oa.oaLib.getOpenLibs():
        lib.getAccess(oa.oaLibAccess(oa.oacReadLibAccess))
    
    ## start the PyQt5 application
    app = QApplication(sys.argv)
    app.setApplicationName("Symbol Viewer Test")
    app.setApplicationVersion("0.0.1")

    ## open the dialog and start the event loop
    form = OASymbolTestDialog()
    form.show()
    app.exec_()
