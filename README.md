**Requirements**

This project will require Si2 OpenAccess and the oaScript extension.
Use of OpenAccess/oaScript requires licenses from Si2.  Please see http://si2.org for details.

It uses Qt5 which is open-source or commerically licensed.  https://www.qt.io
It uses PyQt5 and SIP under open-source or commercial license.  https://www.riverbankcomputing.com/software/pyqt/intro
Qt5 and PyQt5 are easily downloaded and configured through the Anaconda package manager, built into RedHat Linux 
or available at https://anaconda.org

The code in this repository is copyright 2018 by Cory Krug.

**Objective**

This is an example for using Si2 OpenAccess and it's oaScript extension for Python.

Drawing a single symbol is somewhat trivial, but this is for learning/demonstration purposes.  I believe that some tools
should be built/prototyped initially in Python (oaScript/PyQt5), but if more performance is wanted it can be converted to
C++ using OpenAccess/Qt5, then it can still be used within a Python PyQt5 framework.

** Plan **

1. Create an IPython notebook as I explore the API.
2. Create a PyQt5+oaScript QWidget to display an OpenAccess symbol view.
3. Create a basic frame around this widget
4. Convert the widget to C++ OpenAccess
5. Register the widget in Qt5
6. Wrap the widget for PyQt5 using SIP.
7. Show both widgets in PyQt5.


##BitBucket boilerplate code below



**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*


## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).