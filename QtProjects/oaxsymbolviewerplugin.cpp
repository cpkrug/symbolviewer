#include "oaxsymbolviewer.h"
#include "oaxsymbolviewerplugin.h"

#include <QtPlugin>

oaxSymbolViewerPlugin::oaxSymbolViewerPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void oaxSymbolViewerPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool oaxSymbolViewerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *oaxSymbolViewerPlugin::createWidget(QWidget *parent)
{
    return new oaxSymbolViewer(parent);
}

QString oaxSymbolViewerPlugin::name() const
{
    return QLatin1String("oaxSymbolViewer");
}

QString oaxSymbolViewerPlugin::group() const
{
    return QLatin1String("");
}

QIcon oaxSymbolViewerPlugin::icon() const
{
    return QIcon();
}

QString oaxSymbolViewerPlugin::toolTip() const
{
    return QLatin1String("");
}

QString oaxSymbolViewerPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool oaxSymbolViewerPlugin::isContainer() const
{
    return false;
}

QString oaxSymbolViewerPlugin::domXml() const
{
    return QLatin1String("<widget class=\"oaxSymbolViewer\" name=\"oaxSymbolViewer\">\n</widget>\n");
}

QString oaxSymbolViewerPlugin::includeFile() const
{
    return QLatin1String("oaxsymbolviewer.h");
}

