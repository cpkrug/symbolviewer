#ifndef OAXSYMBOLVIEWER_H
#define OAXSYMBOLVIEWER_H

#include <QWidget>
#include <QString>

#include <oaBase.h>
#include <oaDesign.h>
#include <oaDesignDB.h>

class oaxSymbolViewer : public QWidget
{
    Q_OBJECT

public:
    oaxSymbolViewer(QWidget *parent = 0);

    void setOADesign(const QString& lib, const QString& cellname, const QString& viewname);

private:
    OpenAccess_4::oaDesign *design;

protected:
    virtual void paintEvent(QPaintEvent *event);
};

#endif
