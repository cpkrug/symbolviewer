#include "oaxsymbolviewer.h"
#include <QPainter>

#include <algorithm>

oaxSymbolViewer::oaxSymbolViewer(QWidget *parent) :
    QWidget(parent), design(NULL)
{
    setMinimumSize(200, 200);
    setMaximumSize(200, 200);

}

void oaxSymbolViewer::setOADesign(const QString &lib, const QString &cell, const QString &view)
{
    OpenAccess_4::oaNativeNS ns;
    OpenAccess_4::oaScalarName libname(ns,  lib.toLocal8Bit().constData());
    OpenAccess_4::oaScalarName cellname(ns, cell.toLocal8Bit().constData());
    OpenAccess_4::oaScalarName viewname(ns, view.toLocal8Bit().constData());

    OpenAccess_4::oaLib *l = NULL;
    try {
        l = OpenAccess_4::oaLib::find(libname);
        if (l != NULL)
            l->getAccess(OpenAccess_4::oacReadLibAccess);

        design = OpenAccess_4::oaDesign::open(libname, cellname, viewname, 'r');
    } catch (...) {
        // in case of error, NULLify the design, the draw routine will check this
        design = NULL;
    }
    if (l != NULL)
        l->releaseAccess();     // release library access

}

void oaxSymbolViewer::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    // get a painter
    QPainter painter(this);

    //int side = qMin(width(), height());

    if (design ==  NULL || design->getTopBlock() == NULL) {
        painter.setPen(Qt::red);
        painter.drawLine(0, 0, width(), height());

        painter.setPen(Qt::blue);
        painter.drawLine(0, height(), width(), 0);
        return;
    }

    OpenAccess_4::oaBox bb;
    design->getTopBlock()->getBBox(bb);

    double wscale = double(width())  / bb.getWidth();
    double hscale = double(height()) / bb.getHeight();
    double scale = std::min(wscale, hscale);

    // go to the center
    painter.translate(width() / 2.0, height() / 2.0);

    // change scale, invert y axis because OA origin is lower left
    painter.scale(scale, -scale);

    // move to new lower left
    painter.translate(-(bb.left()+bb.right())/2.0, -(bb.bottom()+bb.top())/2.0);

    // paint the background
    painter.setPen(Qt::transparent);
    painter.setBrush(Qt::white);
    painter.drawRect(bb.left(), bb.bottom(), bb.getWidth(), bb.getHeight());

    painter.setPen(Qt::black);
    painter.setBrush(Qt::transparent);

    // paint the cell's shapes
    OpenAccess_4::oaIter<OpenAccess_4::oaShape> sIter(design->getTopBlock()->getShapes());
    while (OpenAccess_4::oaShape *s = sIter.getNext()) {
        int t = s->getType();
        if(t == OpenAccess_4::oacRectType) {
            s->getBBox(bb);
            painter.drawRect(bb.left(), bb.bottom(), bb.getWidth(), bb.getHeight());
        } else if(t == OpenAccess_4::oacLineType) {
            OpenAccess_4::oaPointArray pa;
            static_cast<OpenAccess_4::oaLine *>(s)->getPoints(pa);
            QPoint p0(pa[0].x(), pa[0].y());
            for(unsigned int i=1; i < pa.getNumElements(); ++i) {
                QPoint p1(pa[i].x(), pa[i].y());
                painter.drawLine(p0, p1);
                p0 = p1;
            }
        }
    }
}
