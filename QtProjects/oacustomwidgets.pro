CONFIG      += plugin debug_and_release
TARGET      = $$qtLibraryTarget(oaxcustomwidgetsplugin)
TEMPLATE    = lib

HEADERS     = oaxsymbolviewerplugin.h oaxcustomwidgets.h
SOURCES     = oaxsymbolviewerplugin.cpp oaxcustomwidgets.cpp
RESOURCES   = icons.qrc
LIBS        += -L. 

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS    += target

include(oaxsymbolviewer.pri)

OTHER_FILES += \
    oaxsymbolviewer.sip \
    sipwrap_symbolviewer.py

LIBS += -L/home/ckrug/compiles/oa50p083/lib/linux_rhel50_gcc48x_64/opt
LIBS += -loaCommon -loaPlugIn -loaBase -loaDM -loaUtil -loaDMFileSysBase -loaDMFileSys -loaDMTurboBase
LIBS += -loaDMTurbo  -loaTech -loaDesign -loaNativeText -loaPcellCPP -loaPcellScript -loaRQXYTree -loaWafer

INCLUDEPATH += /home/ckrug/compiles/oa50p083/include/oa
DEPENDPATH += /home/ckrug/compiles/oa50p083/lib/linux_rhel50_gcc48x_64/opt
