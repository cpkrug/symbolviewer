#include "oaxsymbolviewerplugin.h"
#include "oaxcustomwidgets.h"

oaxcustomwidgets::oaxcustomwidgets(QObject *parent)
    : QObject(parent)
{
    m_widgets.append(new oaxSymbolViewerPlugin(this));

}

QList<QDesignerCustomWidgetInterface*> oaxcustomwidgets::customWidgets() const
{
    return m_widgets;
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(oaxcustomwidgetsplugin, oaxcustomwidgets)
#endif // QT_VERSION < 0x050000
